<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Validator;


class UsersController extends Controller
{
    public function index() {
        $users = User::all();
        return view('pages.userList', [
            'users' => $users,
        ]);
    }

    public function create(Request $request, $errorMessages = []) {
    	$templateData = $request->all();
    	$templateData['errorMessages'] = $errorMessages;
        return view('pages.userItem', $templateData);
    }

    public function createPost(Request $request) {
    	$rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ];
        $validator = Validator::make( $request->all(), $rules);
        if( !$validator->fails() ){
        	User::create([
	            'name' => $request->input('name'),
	            'email' => $request->input('email'),
	            'password' => bcrypt($request->input('password')),
        	]);
        	return redirect()->action('UsersController@index');
    	} 

        return $this->create($request, $validator->messages()->all());
	}

	public function edit($userId, Request $request, $errorMessages = []) {
		$user = User::find($userId);
		if(empty($user))
			return redirect()->action('UsersController@index');

		$templateData = $user->toArray();
    	$templateData['errorMessages'] = $errorMessages;
        return view('pages.userItem', $templateData);
	}

	public function editPost($userId, Request $request) {
		$user = User::find($userId);
		if(empty($user))
			return redirect()->action('UsersController@index');

		$rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
        ];
        if(!empty($request->input('password'))) {
        	$rules += [
	        	'password' => 'required|min:6|confirmed',
	            'password_confirmation' => 'required|min:6',
    		];
        }
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->fails()){
        	$user->name = $request->input('name');
        	$user->email = $request->input('email');
        	$user->password = bcrypt($request->input('password'));
			$user->save();
        	return redirect()->action('UsersController@index');
    	}

    	return $this->edit($userId, $request, $validator->messages()->all());
	}

	public function delete($userId) {
		$user = User::find($userId);
		if(!empty($user))
			$user->delete();
		return redirect()->action('UsersController@index');
	}
}
