<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'HomeController@index');
Route::auth();

Route::get('/users','UsersController@index');

Route::get('/users/create','UsersController@create');
Route::post('/users/create','UsersController@createPost');

Route::get('/users/{id}/edit','UsersController@edit');
Route::post('/users/{id}/edit','UsersController@editPost');

Route::get('/users/{id}/delete','UsersController@delete');

