@extends('layouts.container')

@section('content')

	<div class="top-container">
		<h2>Users</h2>
		<a href="/users/create" class="btn btn-primary" >Add</a>
	</div>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            	@foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->created_at}}</td>
                    <td>
                    	<a class="btn btn-warning btn-xs" href="/users/{{$user->id}}/edit">Edit</a>
                    	<a class="btn btn-danger btn-xs buttonDelete" href="/users/{{$user->id}}/delete">Delete</a>
                    </td>
                </tr>                    
                @endforeach    
        </tbody>
    </table>

@endsection
