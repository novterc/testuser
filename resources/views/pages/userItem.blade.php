@extends('layouts.container')

@section('content')

		<h2>User</h2><br/><br/>

		@foreach($errorMessages as $message )
			<div class="alert alert-warning">
			  <strong>Warning!</strong> {{$message}}.
			</div>
		@endforeach

		<form method="POST" class="form-horizontal" role="form">
		    <input type="hidden" name="_token" value="{{ csrf_token() }}">

		    <div class="form-group">
				<label class="control-label col-sm-2" for="name">Name:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="name" value="{{@$name}}" required>
				</div>
		    </div>  

		    <div class="form-group">
				<label class="control-label col-sm-2" for="email">Email:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="email" value="{{@$email}}" required>
				</div>
		    </div>  

		    <div class="form-group">
				<label class="control-label col-sm-2" for="password">Password:</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="password" value="{{@$password}}" >
				</div>
		    </div>  

		    <div class="form-group">
				<label class="control-label col-sm-2" for="passwordConfirm">Password confirm:</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="password_confirmation" value="{{@$password_confirmation}}" >
				</div>
		    </div>	 

		    <div class="form-group">
		    <label class="control-label col-sm-2" for="passwordConfirm"> </label>
				<div class="col-sm-10">
					<input type="submit" class="btn btn-primary">
				</div>
		    </div>	    

		</form>

@endsection
